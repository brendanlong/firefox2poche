if (typeof poche == "undefined") {
	var poche = {
		_prefs: Components.classes["@mozilla.org/preferences-service;1"]
			.getService(Components.interfaces.nsIPrefService).getBranch("extensions.poche@gaulupeau.fr."),

		installButton: function (toolbarId, id, afterId) {
				if (!document.getElementById(id)) {
						var toolbar = document.getElementById(toolbarId);

						// If no afterId is given, then append the item to the toolbar
						var before = null;
						if (afterId) {
								var elem = document.getElementById(afterId);
								if (elem && elem.parentNode == toolbar)
										before = elem.nextElementSibling;
						}

						toolbar.insertItem(id, before);
						toolbar.setAttribute("currentset", toolbar.currentSet);
						document.persist(toolbar.id, "currentset");

						if (toolbarId == "addon-bar")
								toolbar.collapsed = false;
				}
		},

		post: function(event) {
			var url = content.document.location.href;
			var request = new XMLHttpRequest();
			request.open("GET", this._prefs.getCharPref("url") + "?action=add&url=" + btoa(url));
			request.send();
			event.stopPropagation();
		},

		open: function(event) {
			gBrowser.selectedTab = gBrowser.addTab(this._prefs.getCharPref("url"));
			event.stopPropagation();
		}
	};

	window.addEventListener("load", function() {
		Application.getExtensions(function(extensions) {
			var extension = extensions.get("poche@gaulupeau.fr");
			if (extension.firstRun) {
				poche.installButton("nav-bar", "poche-toolbar-button");
			}
		});
	}, false);
}
